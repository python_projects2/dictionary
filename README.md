About 
=================================================
A Python Dictionary Application that takes the word as input and predicts the nearest word if the word is no found


Running the Application
=================================================
Downaload the data.json and then run the following commands
$python3 dictionary.py



Example 
==================================================
Enter word: Tes
Is it toes ? Enter Y/y if yes, or N/n if no: Y
The toes of a person considered as a whole.
