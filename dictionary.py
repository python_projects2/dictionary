##Dictionary.py

import json
from difflib import get_close_matches


def SearchDictionary(word):
    w = word.lower()
    if w in dictionary:
        return dictionary[w]
    elif w.title() in dictionary: #if user entered "texas" this will check for "Texas" as well.
        return data[w.title()]
    elif len(get_close_matches(w, dictionary.keys())) > 0:
        userinput = input("Is it %s ? Enter Y/y if yes, or N/n if no: " % get_close_matches(w, dictionary.keys())[0])
        userinput = userinput.upper()
        if userinput == "Y":
            return dictionary[get_close_matches(w, dictionary.keys())[0]]
        elif userinput == "N":
            return "The word is not present in dictionary"
        else:
            return "Sorry check the key you entered."
    else:
        return "The word is not present in dictionary"






dictionary=json.load(open("data.json"))
word = input("Enter word: ")
output = SearchDictionary(word)
if type(output) == list:
    for item in output:
        print(item)
else:
    print(output)
